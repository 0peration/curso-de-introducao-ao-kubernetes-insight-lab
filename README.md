### Apresentação inicial do curso de Introdução ao Kubernetes. Nesta série de vídeos iremos compartilhar os detalhes introdutórios para você iniciar no mundo do Kubernetes, o famoso k8s.

## Roteiro do curso:

Apresentação
#01 Revisão Docker
#02 O que é Kubernetes?
#03 Arquitetura do K8s
#04 Conceitos importantes do K8s
#05 Instalação dos componentes do K8s
#06 Configuração do K8s
#07 ReplicaSet
#08 Deployment
#09 Service
#10 Microservices
